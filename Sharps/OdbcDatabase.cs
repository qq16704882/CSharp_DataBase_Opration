﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Odbc;
using System.Data;
namespace Sharps
{
   public sealed class OdbcDatabase:IDatabase
    {

        private OdbcConnection con=null;
        private static OdbcDatabase db;

        /// <summary>
        /// 实现链接 
        /// </summary>
        /// <param name="conString">ODBC链接字符串</param>
        public void Connect(string conString)
        {
            try
            {
                con = new OdbcConnection(conString);
            }
            catch
            {
                throw ConnectException.getInstanse();
            }
        }

        /// <summary>
        /// 获取此类的实例 (工厂模式)
        /// </summary>
        /// <returns></returns>
        public static OdbcDatabase getInstanse()
        {
            if (db == null)
            {
                db = new OdbcDatabase();

            }
            return db;
        }

        /// <summary>
        /// 实现查询
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable Query(string sql)
        {
            DataTable dt = new DataTable();
            try
            {
                OdbcCommand com = new OdbcCommand(sql, con);
                OdbcDataAdapter adapter = new OdbcDataAdapter(com);
                adapter.Fill(dt);
            }
            catch (Exception)
            {

            }
            return dt;
        }
        /// <summary>
        /// 实现关闭链接
        /// </summary>
        public void Close()
        {
            try
            {
                con.Close();
            }
            catch
            {
            }
        }
        /// <summary>
        /// 执行SQL
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public int Excute(string sql)
        {
            int p = -1;
            try
            {
                p = new OdbcCommand(sql, con).ExecuteNonQuery();
            }
            catch
            {
            }
            return p;
        }
    }
}
